
/* global describe, it, after, before */
'use strict'

const amqp = require('amqplib')
const should = require('should')

let async = require('async')
let isEmpty = require('lodash.isempty')

let Broker = require('../node_modules/reekoh/lib/broker.lib')

let _broker = null
let _app = null
let _channel = null
let _conn = null

let io = require('socket.io-client')
let socket = io.connect('http://127.0.0.1:8081')

describe('SocketIO Channel Test', () => {
  process.env.PORT = 8081
  process.env.BROKER = 'amqp://guest:guest@localhost'
  process.env.PLUGIN_ID = 'demo.channel'
  process.env.INPUT_PIPE = 'demo.inputpipe'
  process.env.COMMAND_RELAYS = 'demo.relay'

  //let ENV_PLUGIN_ID = process.env.PLUGIN_ID
  let ENV_INPUT_PIPE = process.env.INPUT_PIPE
  let ENV_COMMAND_RELAYS = process.env.COMMAND_RELAYS

  before('init', () => {
    _broker = new Broker()
    amqp.connect(process.env.BROKER).then((conn) => {
      _conn = conn
      return conn.createChannel()
    }).then((channel) => {
      _channel = channel
    }).catch((err) => {
      console.log(err)
    })
  })

  after('terminate', function () {
    _conn.close()
  })

  describe('#start', function () {
    it('should start the app', function (done) {
      this.timeout(10000)
      _app = require('../app')
      _app.once('init', done)
    })
  })

  describe('#data', function () {
    it('should be able to serve a client and exchange data', function(done) {
      this.timeout(15000)

      let dummyData = {
        key1: 'value1',
        key2: 121,
        key3: 40,
        device: '527981789267421'
      }

      socket.on(dummyData.device, () => {
          done()
      })

      _channel.sendToQueue(ENV_INPUT_PIPE, new Buffer(JSON.stringify(dummyData)))
    })
  })

  describe('#message', function () {
    it('should be able to receive correct command', function (done) {
      this.timeout(10000)

      _channel.consume(ENV_COMMAND_RELAYS, (msg) => {
        if (!isEmpty(msg)) {
          async.waterfall([
            async.constant(msg.content.toString('utf8')),
            async.asyncify(JSON.parse)
          ], (err, parsed) => {
            should.equal(parsed.devices, '527981789267421')
            should.equal(parsed.deviceGroups, '527981789267421')
            should.equal(parsed.command, 'ACTIVATE')
            done(err)
          })
        }
        _channel.ack(msg)
      }).catch((err) => {
        should.ifError(err)
      })

      socket.emit('message', {
        type: 'message',
        targetDevices: '527981789267421',
        targetDeviceGroups: '527981789267421',
        command: 'ACTIVATE',
      })
    })
  })
})

