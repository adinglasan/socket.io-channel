
'use strict'

const reekoh = require('reekoh')
const plugin = new reekoh.plugins.Channel()

let server = require('http').createServer()
let socket = require('socket.io')(server)

let isPlainObject = require('lodash.isplainobject')
let isEmpty = require('lodash.isempty')

plugin.on('data', (data) => {
  if (!isPlainObject(data)) {
    console.log(data)
    return plugin.logException(new Error(`Invalid data received. Data must be a valid JSON Object or a collection of objects. Data: ${data}`))
  }
  if (isEmpty(data)) {
    return plugin.logException(new Error('Invalid data. Data should not be empty.'))
  }
  socket.emit(data.device, data)
  plugin.log({
    title: 'Data sent through Socket Channel',
    data: data
  })
})

plugin.once('ready', () => {
  server.listen({
    port: plugin.port
  })

  socket.on('error', (err) => {
    plugin.logException(err)
    console.error(err)
  })

  socket.on('connection', (sock) => {
    sock.on('error_connection', (error) => {
      plugin.logException(error)
      console.log(error)
    })

    sock.on('message', (obj, error) => {
      if (error) {
        return plugin.logException(new Error('Invalid command received. Command must not be empty.'))
      }
      if (isEmpty(obj.targetDevices)) {
        return plugin.logException(new Error('Invalid targetDevices. Target Devices should not be empty.'))
      }
      if (obj.type === 'message') {
        plugin.relayCommand(obj.command, obj.targetDevices, obj.targetDeviceGroups, obj.device).then(() => {
          return plugin.log(JSON.stringify({
            title: 'Socket.IO - Message Received',
            message: obj
          }))
        }).catch(plugin.logException)
      }
    })
  })
  plugin.log(`Channel has been initialized on port ${plugin.port}`)
  plugin.emit('init')
})
module.exports = plugin
