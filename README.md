# Socket.IO Channel

## Description
The SocketIO Channel Plugin for Reekoh enables applications to subscribe to the real-time feed of data from the devices while also giving applications the ability to send messages/commands to devices or group of devices which the device can perform.

### Configuration

- **Channel Name** - This is a label given to your plugin to locate it easily in your pipeline.
- **Message Event** - The message type to filter for device messages or commands. Default: message.

![](https://reekohdocs.blob.core.windows.net/plugin-docs/channel/socket.io-channel/1.0.0/socket-configuration.png)

## Send Data

In order to simulate sending data, You will need a Gateway Plugin to send the data to Socket.IO Channel Plugin. In the screenshot below, it uses MQTT Gateway Plugin. You need to install or download a MQTT client simulator. One example of this is **MQTT BOX**. When you create a connection for your device in MQTTBox, you will be required to provide the following details:

- **MQTT Client Name** - This is the name of your device connection.
- **Protocol** - This is the assigned protocol IP address of MQTT Gateway in your pipeline.
- **Host** - This is the assigned IP address and Port of MQTT Gateway in your pipeline.
- **MQTT Client ID** - This is the device ID which is registered in Reekoh's Devices Management.

![](https://reekohdocs.blob.core.windows.net/plugin-docs/channel/socket.io-channel/1.0.0/socket-mqtt.png)

Make sure your plugins and pipeline are successfully deployed.

![](https://reekohdocs.blob.core.windows.net/plugin-docs/channel/socket.io-channel/1.0.0/socket-pipeline.png)

Using **MQTT Box** as MQTT Client simulator. Click **Save** you can now simulate sending data.

![](https://reekohdocs.blob.core.windows.net/plugin-docs/channel/socket.io-channel/1.0.0/socket-data.png)

## Sample input data

The MQTT Gateway only accepts data in JSON format. A device is required to be in the MQTT Client ID field. This field should contain a device ID which is registered in Reekoh's Devices Management. The example below is data to be sent.
``` javascript
{
    "data": "This is a test data from MQTT Gateway."
}
```

## Verify Data

The data will be ingested by the MQTT Gateway plugin, which will be forwarded to all the other plugins that are connected to it in the pipeline.

To verify if the data is ingested properly in the pipeline, you need to check the **LOGS** tab in every plugin in the pipeline. All the data that have passed through the plugin will be logged in the **LOGS** tab.
If an error occurs, an error exception will be logged in the **EXCEPTIONS** tab.

![](https://reekohdocs.blob.core.windows.net/plugin-docs/channel/socket.io-channel/1.0.0/socket-datalogs.png)


## Send Command

To send command you need to install or download a Socket.IO client simulator. One example of this is **Socket.IO Tester**. When you create a connection for your device in Socket.IO Tester, you will be required to provide the following details:

- **URL** - This is the assigned IP address and Port of Socket.IO Channel in your pipeline.
- **Send Message** - Message Event you've specified upon configuration of your Socket.IO Channel plugin.
- **Listen for Events** - To verify if the command is sent.

![](https://reekohdocs.blob.core.windows.net/plugin-docs/channel/socket.io-channel/1.0.0/socket-tester.png)

## Sample Input Command
``` javascript
{
    "type":"message",
    "command":"ACTIVATE",
    "targetDevices":"5118bb89-43ab-5da6-9eec-cce8b07e1e9f",
    "targetDeviceGroups":"89d60574-d29b-5d82-9a51-ae2b9924bbbf"
}

```

## Verify Command

The command will be ingested by the Socket.IO Channel plugin, which will be forwarded to the command relay.The command relay will relay the commands to all the other plugins that are connected to it in the pipeline.

To verify if the command is ingested properly in the pipeline, you need to check the **LOGS** tab in Socket.IO Channel. The command that passed through the command relay will be sent to MQTT gateway.

![](https://reekohdocs.blob.core.windows.net/plugin-docs/channel/socket.io-channel/1.0.0/socket-logs.png)


